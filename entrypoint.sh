#!/bin/sh

set -e 

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
#run nginx in foreground. log will be put
nginx -g 'daemon off;'

